# xZecora's fork of st

Patches applied include:
- anysize
- universcroll
- scrollback
- scrollback-mouse
- alpha
- font2
- Xresources
- blinking-cursor
- ligatures
